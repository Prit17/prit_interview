//
//  APIManager.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import Foundation
import  UIKit

struct APIBaseURL {
    static let baseURL = "https://newsapi.org/v2"
}

struct APIEndPoints {
    static let newsEndPoint = "/top-headlines?"
}

class APIManager {
    static let shared = APIManager(url: APIBaseURL.baseURL)
    
    var baseURL : String?
    
    private init(url: String){
        self.baseURL = url
    }
    
    
    func getNewsFeed(completion: @escaping ([NewsModel], Error?) -> ()) {
        
        let session = URLSession.shared
        let url = URL(string: self.baseURL! + APIEndPoints.newsEndPoint + "sources=google-news&apiKey=\("7ef6c6ad69394f70947a9f4a83b37864")")!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            
            // Check if an error occured
            if error != nil {
                // HERE you can manage the error
                return
            }
            
//            // Serialize the data into an object
            do {
                //let json = try JSONDecoder().decode([NewsModel].self, from: data!)
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                print(json)
                
                if let resultDic = json as? [String : Any]{
                    
                    if let status = resultDic["status"] as? String{
                        
                        if status == "ok"{
                            
                            if let arrArticles = resultDic["articles"] as? [[String : Any]]{
                                
                                let modelArr = arrArticles.map { return NewsModel(dic: $0) }
                                if modelArr.count != 0{
                                    DispatchQueue.main.async {
                                        completion(modelArr, nil)
                                    }
                                }
                            
                            }
                            
                        }
                        
                    }
                        
                }
            } catch {
                print("Error during JSON serialization: \(error.localizedDescription)")
            }
            
        })
        task.resume()
    }
}
