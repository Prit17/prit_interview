//
//  NewsModel.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import Foundation
import UIKit

class NewsModel {
    var author : String?
    var description : String?
    var publishedAt : String?
    var title : String?
    var url : String?
    var urlToImage : String?
    var content : String?
    
    init(dic: [String : Any]) {
        
        if let author = dic["author"] as? String{
            self.author = author
        }
        
        if let _description = dic["description"] as? String{
            self.description = _description
        }
        
        if let publishedAt = dic["publishedAt"] as? String{
            self.publishedAt = publishedAt
        }
        
        if let title = dic["title"] as? String{
            self.title = title
        }
        
        if let url = dic["url"] as? String{
            self.url = url
        }
        
        if let urlToImage = dic["urlToImage"] as? String{
            self.urlToImage = urlToImage
        }
        
        if let content = dic["content"] as? String{
            self.content = content
        }
        
    }
}
