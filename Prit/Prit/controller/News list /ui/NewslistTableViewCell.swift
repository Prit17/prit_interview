//
//  NewslistTableViewCell.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import UIKit
import SDWebImage

class NewslistTableViewCell: UITableViewCell {
    @IBOutlet weak var labelNewsTitle: UILabel!
    @IBOutlet weak var labelAutherName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelNewsLink: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    
    
    var newsVM : NewsViewModel!{
        didSet{
            self.labelNewsTitle.text = newsVM.title
            
            self.labelAutherName.text = newsVM.author
            
            self.labelDate.text = Date.getFormattedDate(string: newsVM.publishedAt, formatter: DateFormate.YYYY_MM_DD_T_HH_MM_SS.rawValue, newFormate: DateFormate.DD_MMM_YYYY_HH_MM_A.rawValue)            
            
            self.labelNewsLink.text = newsVM.url
            self.labelNewsLink.addUnderLine()
            self.labelNewsLink.addTapGestureRecognizer{
                
                if let url = URL(string: self.newsVM.url){
                    if UIApplication.shared.canOpenURL(url)
                    {
                        DispatchQueue.main.async {
                            UIApplication.shared.open(URL(string: self.newsVM.url)!, options: [:], completionHandler: nil)
                        }
                    }
                }
                
            }
            
            if let url = URL(string: newsVM.urlToImage) as URL?{
                self.imgNews.sd_setImage(with: url, completed: nil)
            }else{
                self.imgNews.image = nil
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
