//
//  NewsViewModel.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import Foundation
import UIKit

struct NewsViewModel {
    
    var author : String
    var description : String
    var publishedAt : String
    var title : String
    var content : String
    var url : String
    var urlToImage : String
    
    init(model: NewsModel) {
        self.author = model.author ?? ""
        self.description = model.description ?? ""
        self.publishedAt = model.publishedAt ?? ""
        self.title = model.title ?? ""
        self.url = model.url ?? ""
        self.urlToImage = model.urlToImage ?? ""
        self.content = model.content ?? ""
    }
}
