//
//  ViewController.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import UIKit

class NewslistController: UIViewController {

    //MARK:- IBOutles
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:- Vars
    var arrNewsViewModel = [NewsViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadXIB()
    }


    //MARK:- Set UI
    private func loadXIB(){
        self.tblView.register(UINib(nibName: "NewslistTableViewCell", bundle: nil), forCellReuseIdentifier: "NewslistTableViewCell")
        
        self.tblView.addSpinner()
        getAllNewsFeed()
    }
    
    //MARK:- Navigation
    private func navigateToNewsDetails(index: Int){
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailsController") as? NewsDetailsController else { return }
        vc.newsVM = self.arrNewsViewModel[index]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- API Call
    private func getAllNewsFeed(){
        
        APIManager.shared.getNewsFeed { (newsModel, error) in
            
            self.arrNewsViewModel = newsModel.map{ return NewsViewModel(model: $0) }
            
            DispatchQueue.main.async {
                self.tblView.reloadData()
                self.tblView.removeSpinner()
            }
            
        }
        
    }
    
}

extension NewslistController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNewsViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tblView.dequeueReusableCell(withIdentifier: "NewslistTableViewCell") as? NewslistTableViewCell else { return UITableViewCell() }
        cell.newsVM = self.arrNewsViewModel[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateToNewsDetails(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
