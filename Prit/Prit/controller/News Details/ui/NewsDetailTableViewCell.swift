//
//  NewsDetailTableViewCell.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import UIKit

class NewsDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var labelNewsTitle: UILabel!
    @IBOutlet weak var labelAutherName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    var newsVM : NewsViewModel!{
        didSet{
            self.labelNewsTitle.text = newsVM.title
            
            self.labelAutherName.text = newsVM.author
            
            self.labelDate.text = Date.getFormattedDate(string: newsVM.publishedAt, formatter: DateFormate.YYYY_MM_DD_T_HH_MM_SS.rawValue, newFormate: DateFormate.DD_MMM_YYYY_HH_MM_A.rawValue)
                    
            if let url = URL(string: newsVM.urlToImage) as URL?{
                self.imgNews.sd_setImage(with: url, completed: nil)
            }else{
                self.imgNews.image = nil
            }
            
            self.labelDescription.text = newsVM.description
            self.labelContent.text = newsVM.content
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
