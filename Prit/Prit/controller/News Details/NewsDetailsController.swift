//
//  NewsDetailsController.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import UIKit

class NewsDetailsController: UIViewController {

    //MARK:- IBOutles
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:- Vars
    var newsVM : NewsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadXIB()
    }

    
    //MARK:- Set UI
    private func loadXIB(){
        self.tblView.register(UINib(nibName: "NewsDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsDetailTableViewCell")
        
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
}


extension NewsDetailsController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tblView.dequeueReusableCell(withIdentifier: "NewsDetailTableViewCell") as? NewsDetailTableViewCell else { return UITableViewCell() }
        cell.newsVM = self.newsVM
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
