//
//  Utilities.swift
//  Prit
//
//  Created by PK on 13/02/21.
//  Copyright © 2021 PK. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    
    func  addUnderLine(){
        guard let text = self.text else { return }
        let textRange = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        attributedText.addAttribute(.foregroundColor,
                                    value: UIColor.systemBlue,
        range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
    }
}

extension Date{
    
    static func getFormattedDate(string: String , formatter:String, newFormate: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = newFormate
        let date: Date? = dateFormatterGet.date(from: string)
        return dateFormatterPrint.string(from: date!);
    }
    
}


enum DateFormate : String{
    case YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ssZ"
    case DD_MMM_YYYY_HH_MM_A = "dd MMM, yyyy hh:mm a"
    
}


extension UIView {
    
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
        }
    }
    
}


extension UITableView{
     func addSpinner() {
        
        let spinner = UIActivityIndicatorView()
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.style = .medium
        spinner.color = .gray
        spinner.startAnimating()
        self.backgroundView = spinner

    }

    // Remove the activity indicator from the main view
     func removeSpinner() {
        self.backgroundView = nil
    }
}
